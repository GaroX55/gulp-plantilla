# PLANTILLA DESARROLLO FRONT END #

Plantilla creada para el manejo del Front End

Usamos GULP + BOWER + LESS

La plantilla levanta un servidor local (livereload) concatena y ofusca los CSS Y JS

## Instalación

Al cargar el proyecto debemos abrir la consola (terminal o VisualStudio Code terminal)

**PASOS**

Instalamos las dependencias de NPM:

```
npm install
```

Instalamos las dependencias de BOWER
```
Bower install
```
Si estas en un **MAC** y estas como super usuario (sudo), debes agregarle --allow-root

```
Bower install --allow-root
```
**INICIAR**

En la terminal ejecutamos el proceso de gulp

```
Gulp
```

